<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hongblog
 */

$back_to_top = hongblog_get_theme_option( 'back_to_top' , '1' );

?>			
			</div><!-- .content-wrap -->
		</div><!-- .inner -->
	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<?php get_sidebar( 'footer' ); ?>
		<div class="footer-wrap">
			<div class="inner clearfix">
				<div class="site-info">
					<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'hongblog' ) ); ?>">
						<?php
						/* translators: %s: CMS name, i.e. WordPress. */
						printf( esc_html__( 'Proudly powered by %s', 'hongblog' ), 'WordPress' );
						?>
					</a>
					<span class="sep"> | </span>
						<?php
						/* translators: 1: Theme name, 2: Theme author. */
						printf( esc_html__( 'Theme: %1$s by %2$s.', 'hongblog' ), 'hongblog', '<a href="https://themecountry.com/">ThemeCountry</a>' );
						?>
				</div><!-- .site-info -->
			</div><!-- .inner -->
		</div>
			
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php if ( $back_to_top == 1) { ?> <a id="move-to-top" class="animate  filling" href="#blog">
	<i class="fa fa-angle-up"></i>
</a><!-- #move-to-top --> <?php } ?> 

<?php wp_footer(); ?>

</body>
</html>
