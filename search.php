<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package hongblog
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">	

				<?php if ( have_posts() ) : ?>

					<header class="page-header">
						<h1 class="page-title">
							<?php
							/* translators: %s: search query. */
							printf( esc_html__( 'Search Results for: %s', 'hongblog' ), '<span>' . get_search_query() . '</span>' );
							?>
						</h1>
					</header><!-- .page-header -->

					<div class="post-wrapper">

					<?php

					$post_layout = hongblog_get_theme_option('post_layout', '');
					$post_layout == 'grid' ? 'grid' : get_post_type();

					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						get_template_part( 'template-parts/content', $post_layout );

					endwhile;

					echo '</div> <!-- .post-wrapper -->';			

					hongblog_paging_nav();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
