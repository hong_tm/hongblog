<?php
/**
 * Template part for displaying content single.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Mekanews
 */
//$theme_options = hongblog_theme_options();
//$position = array_flip($theme_options['sharing_button_position']);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

	<?php hongblog_yoast_breadcrumb(); ?>


	<?php if ( $theme_options['featured_post_single_page'] == 1 ) : ?>
		
		<div class="post-thumbnail">
			
			<?php if ( has_post_thumbnail() ) : ?>
				
				<?php the_post_thumbnail('featured-post-thumbnails'); ?>
			
			<?php else : ?>
				
				<img src="<?php echo get_template_directory_uri(); ?>/images/featured-post-thumbnails.jpg" />
			
			<?php endif; ?>

		</div><!-- .post-thumbnail -->

	<?php endif; ?>

	<header class="entry-header">
		<?php
			the_title( '<h1 class="entry-title single-title">', '</h1>' );

		if ( 'post' === get_post_type() ) :
			
			//$meta_items = array_flip( $theme_options['post_meta_info'] );	
			?>
		
				<div class="entry-meta">			
					<?php hongblog_posted_on() ?>
					<?php hongblog_entry_footer() ?>
				</div><!-- .entry-meta -->
			<?php
			endif;
			
			?>

	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			//the_content();
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'hongblog' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'hongblog' ),
				'after'  => '</div>',
			) );
		?>
		
	</div><!-- .entry-content -->

</article><!-- #post-## -->
