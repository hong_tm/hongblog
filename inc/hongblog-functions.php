<?php 

if ( ! function_exists('hongblog_get_theme_option') ) :

	/**
	 * Get Theme Options
	 * 
	 * @param  mix $setting
	 * @param  mix $default
	 * @return mix
	 */
	function hongblog_get_theme_option( $setting, $default = '' ) {

	    $options = get_option( 'hongblog_options', array() );

	    $value = $default;

	    if ( isset( $options[ $setting ] ) ) {

	        $value = $options[ $setting ];

	    }

	    return $value;
	}

endif;

if ( ! function_exists( 'hongblog_theme_info' ) ) :

	/**
	 * Get Theme Options
	 * 
	 * @param  mix $setting
	 * @param  mix $default
	 * @return mix
	 */
	function hongblog_theme_info() {

		if ( function_exists( 'wp_get_theme' ) ) {

			return wp_get_theme( get_hongblog_name() );

		}

		return false;

	}

endif;

