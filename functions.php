<?php
/**
 * hongblog functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package hongblog
 */

if ( ! function_exists( 'hongblog_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function hongblog_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on hongblog, use a find and replace
		 * to change 'hongblog' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'hongblog', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'hongblog' ),
		) );

		register_nav_menus( array(
			'social-menu' => esc_html__( 'Social Menu', 'hongblog' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'hongblog_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 320, 200, true ); //post thumbnails grid
		add_image_size( 'widget-thumbnails', 70 , 65, true ); 
		add_image_size( 'feature-thumbnails', 740 , 492, true ); 
		add_image_size( 'post-list-thumbnails', 236 , 157, true );
		

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 36,
			'width'       => 170,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'hongblog_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function hongblog_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'hongblog_content_width', 640 );
}
add_action( 'after_setup_theme', 'hongblog_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function hongblog_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'hongblog' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'hongblog' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'hongblog' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'hongblog' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'hongblog' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'hongblog' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'hongblog' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'hongblog' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'Feature Sidebar', 'hongblog' ),
		'id'            => 'feature-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'hongblog' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '',
		'after_title'   => '',
	) );


}
add_action( 'widgets_init', 'hongblog_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function hongblog_scripts() {
	wp_enqueue_style( 'hongblog-open-sans', '//fonts.googleapis.com/css?family=Open+Sans:300,400,700');
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'hongblog-style', get_stylesheet_uri() );

	wp_enqueue_style( 'hongblog-responsive', get_template_directory_uri() .  '/css/responsive.css');

	wp_enqueue_style( 'hongblog-owl-carousel', get_template_directory_uri() .  '/css/owl.carousel.css');

	wp_enqueue_script( 'carousel-script', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '20151215', true );

	wp_enqueue_script( 'hongblog-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'hongblog-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'hongblog-script', get_template_directory_uri() . '/js/script.js',array('jquery'), '20151215', true );

	


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'hongblog_scripts' );


/*
|------------------------------------------------------------------------------
| Truncate string to x letters/words
|------------------------------------------------------------------------------
*/

function hongblog_truncate( $str, $length = 40, $units = 'letters', $ellipsis = '&nbsp;&hellip;' ) {
    if ( $units == 'letters' ) {
        if ( mb_strlen( $str ) > $length ) {
            return mb_substr( $str, 0, $length ) . $ellipsis;
        } else {
            return $str;
        }
    } else {
        $words = explode( ' ', $str );
        if ( count( $words ) > $length ) {
            return implode( " ", array_slice( $words, 0, $length ) ) . $ellipsis;
        } else {
            return $str;
        }
    }
}

if ( ! function_exists( 'hongblog_excerpt' ) ) {
    function hongblog_excerpt( $limit = 40 ) {
      return hongblog_truncate( get_the_excerpt(), $limit, 'words' );
    }
}

/**
 * ------------------------------------------------------------------
 * Customizer Options
 * ------------------------------------------------------------------
 * 
 * Include the Kirki library
 */

require_once get_template_directory() . '/inc/customizer/include-kirki.php';

require_once get_template_directory() .      '/inc/customizer/class-customizer.php';

require_once get_template_directory() . '/inc/hongblog-functions.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom extra for this theme.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

// Include Popular Posts Widget
require get_template_directory() . '/inc/widget/popular.php';

// Include Recent Posts Widget
require get_template_directory() . '/inc/widget/recent-posts.php';
/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

