<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package hongblog
 */

$excerpt = hongblog_get_theme_option( 'post_length' , 'excerpt' );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post-grid'); ?>>

	<div class="content-wrapper">

		<div class="post-thumbnail">
			<a href="<?php the_permalink() ?>" rel="bookmark" class="featured-thumbnail">
			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail('post-list-thumbnails');  ?>
			<?php else : ?>
				<img src="<?php echo get_template_directory_uri(); ?>/images/post-thumbnails-list.png" />
			<?php endif; ?>
			</a>
		</div><!-- .post-thumbnail -->

		<div class="post-content">
		
			<header class="entry-header">
				<?php
				if ( is_singular() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;

				if ( 'post' === get_post_type() ) :
					?>
					<div class="entry-meta">
						<?php
						hongblog_posted_on();
						//hongblog_posted_by();
						?>
					</div><!-- .entry-meta -->
				<?php endif; ?>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php

				if ($excerpt == 'excerpt') {
					the_excerpt();
				} else {
					the_content();
				}

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'hongblog' ),
					'after'  => '</div>',
				) );
				?>
			</div><!-- .entry-content -->

		</div> <!-- .post-content -->

	</div> <!-- .contend-wrapper -->
</article><!-- #post-<?php //the_ID(); ?> -->
