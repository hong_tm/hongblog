<?php
/**
 * Sanitize Functions
 * 
 */

/**
 * Sanitize textare code (JS, CSS, HTML)
 * @param  string $text 
 * @return string
 */
function hongblog_sanitize_textarea_code( $text ) {

	return esc_textarea( $text );

}