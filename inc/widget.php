<?php

// Include Popular Posts Widget
require get_template_directory() . '/inc/widget/popular.php';

// Include Recent Posts Widget
require get_template_directory() . '/inc/widget/recent-posts.php';