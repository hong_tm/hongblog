<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hongblog
 */

$sticky = hongblog_get_theme_option( 'sticky_menu' , '1' );

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site-container">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'hongblog' ); ?></a>

	<header id="masthead" class="site-header">
		
		<div class="header-wrapper">
			<div class="inner clearfix">
				<div class="site-branding">
				<?php hongblog_header_title() ?>
				</div><!-- .site-branding -->
				<nav class="social-navigation">
					<?php 
					if ( has_nav_menu( 'social-menu' ) ) :
						wp_nav_menu( array( 
							'theme_location' => 'social-menu',
							'menu_class' => 'social-menu',
							'container'      => ''	
						) );
					endif;	 
					?>	
				</nav>
			</div>			
		</div>

		<div <?php if ( $sticky == 1) { ?> id="sticky" <?php } ?> class="main-menu">
			<div class="inner clearfix">
				<nav id="site-navigation" class="main-navigation">
					<a class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'hongblog' ); ?></a>
						<?php
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_id'        => 'primary-menu',
							'menu_class'     => 'primary-menu',
							'container'      => ''	
						) );
						?>
				</nav><!-- #site-navigation -->
				<div class="header-search">
					<form method="get" id="searchform" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" _lpchecked="1">
					<input type="text" name="s" id="s" value="<?php echo get_search_query(); ?>" placeholder="<?php esc_html_e('Search ...', 'hongblog'); ?>" />
					<button type="submit" class="submit" name="submit"/>
						<i class="fa fa-search"></i>
					</button>
					</form>
				</div>
			</div>

		</div>
		<div id="catcher"></div>
		
	</header><!-- #masthead -->

	<?php 
		$align = hongblog_get_theme_option( 'sidebar_layout', 'none' );

		//var_dump($align);
	?>

	<div id="content" class="site-content">
		<div class="inner clearfix">

			<?php if ( is_home() && !is_paged() ) : ?>		

			<?php echo hongblog_latest_posts_slider(); ?>

			<?php endif; ?>
			<div class="content-wrap">
