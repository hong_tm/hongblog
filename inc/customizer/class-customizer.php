<?php
/**
 * Shopper Customizer Class
 *
 * 
 * @package  Shopper
 * @author   WooThemes
 * @author   ShopperWP
 * @since    1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Hongblog_Customizer' ) ) :

	/**
	 * The Shopper Customizer class
	 */
	class Hongblog_Customizer {

		/**
		 * Setup class.
		 *
		 * @since 1.0.0
		 */
		public function __construct() {

			$this->token = 'hongblog';
			$this->conf_id = $this->token . '_conf_id';
			$this->panel_id = $this->token . '_panel_id';
			$this->includes();

			add_action( 'init', array( $this , 'customize_register' ) );
			add_action( 'customize_register', array($this, 'register_custom_controls') );
		
		}

		public function register_custom_controls( $wp_customize ) {		

			require_once get_template_directory() . '/inc/customizer/class-custom-code-control.php';
			add_filter( 'kirki/control_types', 'tc_custom_code_control');

		}

	

		/**
		 * Add postMessage support for site title and description for the Theme Customizer along with several other settings.
		 *
		 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
		 * @since  1.0.0
		 */
		public function customize_register() {

			if ( ! class_exists( 'Hongblog_Kirki' ) ) 			
			return;

			

			// Load customize options from file
			require_once get_template_directory() . '/inc/customizer/customize-options.php';

			// Kirki Config
			add_filter( 'kirki/my_config/l10n', 	array( $this, 'kirki_ltenn' ) );

			Hongblog_Kirki::add_config( $this->conf_id, array(
				'option_type'	=> 'option',
				'option_name'	=> 'Hongblog_options',
				'capability'	=> 'edit_theme_options',
			) );

			$this->_add_panel();
			$this->_add_sections();
			$this->_add_fields();

			add_filter('Hongblog_remove_customize_control', array($this, 'remove_controls'));

		}


		public function remove_controls( $controls ) {

			$controls[] = 'Hongblog_homepage_control';

			return $controls;
		}

		private function _add_panel() {
			
			Hongblog_Kirki::add_panel( $this->panel_id, array(
			    'priority'    => 200,
			    'title'       => __( 'Theme Options', 'hongblog' )
			) );
		}

		private function _add_sections() {

			$sections = Hongblog_get_default_sections( $this->token );	

			if ( ! empty( $sections ) ) {

				foreach ($sections as $key => $section) {

					Hongblog_Kirki::add_section( $key, $section);

				}
			}
			
		}

		private function _add_fields() {

			$fields = Hongblog_get_default_fields( $this->token );

			if ( ! empty( $fields ) ) {

				foreach ($fields as $field) {

					Hongblog_Kirki::add_field( $this->conf_id, $field);

				}
			}

		}

		public function Hongblog_options() {
			// Merge Theme Options Array from Database with Default Options Array
			$theme_options = wp_parse_args( 
				
				// Get saved theme options from WP database
				get_option( 'Hongblog_options', array() ), 
				
				// Merge with Default Options if setting was not saved yet
				Hongblog__default_options()
				
			);

			// Return theme options
			return $theme_options;

		}


		public function Hongblog_get_option( $setting, $default ) {

	    	$options = get_option( 'Hongblog_options' );

	    	$value = $default;

		    if ( isset( $options[ $setting ] ) ) {
		        $value = $options[ $setting ];
		    }

	    	return $value;

		}

		private function includes() {
			require_once  get_template_directory() . '/inc/customizer/class-kirki.php';
			
		}

		/**
		 * Translating Kirki strings 
		 * 
		 * @param  		array $l10n
		 * @since 		1.0.0
		 * @return 		array 
		 */
		public static function kirki_ltenn( $l10n ) {

			$l10n['background-color']      = esc_attr__( 'Background Color', 'hongblog' );
			$l10n['background-image']      = esc_attr__( 'Background Image', 'hongblog' );
			$l10n['no-repeat']             = esc_attr__( 'No Repeat', 'hongblog' );
			$l10n['repeat-all']            = esc_attr__( 'Repeat All', 'hongblog' );
			$l10n['repeat-x']              = esc_attr__( 'Repeat Horizontally', 'hongblog' );
			$l10n['repeat-y']              = esc_attr__( 'Repeat Vertically', 'hongblog' );
			$l10n['inherit']               = esc_attr__( 'Inherit', 'hongblog' );
			$l10n['background-repeat']     = esc_attr__( 'Background Repeat', 'hongblog' );
			$l10n['cover']                 = esc_attr__( 'Cover', 'hongblog' );
			$l10n['contain']               = esc_attr__( 'Contain', 'hongblog' );
			$l10n['background-size']       = esc_attr__( 'Background Size', 'hongblog' );
			$l10n['fixed']                 = esc_attr__( 'Fixed', 'hongblog' );
			$l10n['scroll']                = esc_attr__( 'Scroll', 'hongblog' );
			$l10n['background-attachment'] = esc_attr__( 'Background Attachment', 'hongblog' );
			$l10n['left-top']              = esc_attr__( 'Left Top', 'hongblog' );
			$l10n['left-center']           = esc_attr__( 'Left Center', 'hongblog' );
			$l10n['left-bottom']           = esc_attr__( 'Left Bottom', 'hongblog' );
			$l10n['right-top']             = esc_attr__( 'Right Top', 'hongblog' );
			$l10n['right-center']          = esc_attr__( 'Right Center', 'hongblog' );
			$l10n['right-bottom']          = esc_attr__( 'Right Bottom', 'hongblog' );
			$l10n['center-top']            = esc_attr__( 'Center Top', 'hongblog' );
			$l10n['center-center']         = esc_attr__( 'Center Center', 'hongblog' );
			$l10n['center-bottom']         = esc_attr__( 'Center Bottom', 'hongblog' );
			$l10n['background-position']   = esc_attr__( 'Background Position', 'hongblog' );
			$l10n['background-opacity']    = esc_attr__( 'Background Opacity', 'hongblog' );
			$l10n['on']                    = esc_attr__( 'ON', 'hongblog' );
			$l10n['off']                   = esc_attr__( 'OFF', 'hongblog' );
			$l10n['all']                   = esc_attr__( 'All', 'hongblog' );
			$l10n['cyrillic']              = esc_attr__( 'Cyrillic', 'hongblog' );
			$l10n['cyrillic-ext']          = esc_attr__( 'Cyrillic Extended', 'hongblog' );
			$l10n['devanagari']            = esc_attr__( 'Devanagari', 'hongblog' );
			$l10n['greek']                 = esc_attr__( 'Greek', 'hongblog' );
			$l10n['greek-ext']             = esc_attr__( 'Greek Extended', 'hongblog' );
			$l10n['khmer']                 = esc_attr__( 'Khmer', 'hongblog' );
			$l10n['latin']                 = esc_attr__( 'Latin', 'hongblog' );
			$l10n['latin-ext']             = esc_attr__( 'Latin Extended', 'hongblog' );
			$l10n['vietnamese']            = esc_attr__( 'Vietnamese', 'hongblog' );
			$l10n['hebrew']                = esc_attr__( 'Hebrew', 'hongblog' );
			$l10n['arabic']                = esc_attr__( 'Arabic', 'hongblog' );
			$l10n['bengali']               = esc_attr__( 'Bengali', 'hongblog' );
			$l10n['gujarati']              = esc_attr__( 'Gujarati', 'hongblog' );
			$l10n['tamil']                 = esc_attr__( 'Tamil', 'hongblog' );
			$l10n['telugu']                = esc_attr__( 'Telugu', 'hongblog' );
			$l10n['thai']                  = esc_attr__( 'Thai', 'hongblog' );
			$l10n['serif']                 = _x( 'Serif', 'font style', 'hongblog' );
			$l10n['sans-serif']            = _x( 'Sans Serif', 'font style', 'hongblog' );
			$l10n['monospace']             = _x( 'Monospace', 'font style', 'hongblog' );
			$l10n['font-family']           = esc_attr__( 'Font Family', 'hongblog' );
			$l10n['font-size']             = esc_attr__( 'Font Size', 'hongblog' );
			$l10n['font-weight']           = esc_attr__( 'Font Weight', 'hongblog' );
			$l10n['line-height']           = esc_attr__( 'Line Height', 'hongblog' );
			$l10n['font-style']            = esc_attr__( 'Font Style', 'hongblog' );
			$l10n['letter-spacing']        = esc_attr__( 'Letter Spacing', 'hongblog' );
			$l10n['top']                   = esc_attr__( 'Top', 'hongblog' );
			$l10n['bottom']                = esc_attr__( 'Bottom', 'hongblog' );
			$l10n['left']                  = esc_attr__( 'Left', 'hongblog' );
			$l10n['right']                 = esc_attr__( 'Right', 'hongblog' );
			$l10n['color']                 = esc_attr__( 'Color', 'hongblog' );
			$l10n['add-image']             = esc_attr__( 'Add Image', 'hongblog' );
			$l10n['change-image']          = esc_attr__( 'Change Image', 'hongblog' );
			$l10n['remove']                = esc_attr__( 'Remove', 'hongblog' );
			$l10n['no-image-selected']     = esc_attr__( 'No Image Selected', 'hongblog' );
			$l10n['select-font-family']    = esc_attr__( 'Select a font-family', 'hongblog' );
			$l10n['variant']               = esc_attr__( 'Variant', 'hongblog' );
			$l10n['subsets']               = esc_attr__( 'Subset', 'hongblog' );
			$l10n['size']                  = esc_attr__( 'Size', 'hongblog' );
			$l10n['height']                = esc_attr__( 'Height', 'hongblog' );
			$l10n['spacing']               = esc_attr__( 'Spacing', 'hongblog' );
			$l10n['ultra-light']           = esc_attr__( 'Ultra-Light 100', 'hongblog' );
			$l10n['ultra-light-italic']    = esc_attr__( 'Ultra-Light 100 Italic', 'hongblog' );
			$l10n['light']                 = esc_attr__( 'Light 200', 'hongblog' );
			$l10n['light-italic']          = esc_attr__( 'Light 200 Italic', 'hongblog' );
			$l10n['book']                  = esc_attr__( 'Book 300', 'hongblog' );
			$l10n['book-italic']           = esc_attr__( 'Book 300 Italic', 'hongblog' );
			$l10n['regular']               = esc_attr__( 'Normal 400', 'hongblog' );
			$l10n['italic']                = esc_attr__( 'Normal 400 Italic', 'hongblog' );
			$l10n['medium']                = esc_attr__( 'Medium 500', 'hongblog' );
			$l10n['medium-italic']         = esc_attr__( 'Medium 500 Italic', 'hongblog' );
			$l10n['semi-bold']             = esc_attr__( 'Semi-Bold 600', 'hongblog' );
			$l10n['semi-bold-italic']      = esc_attr__( 'Semi-Bold 600 Italic', 'hongblog' );
			$l10n['bold']                  = esc_attr__( 'Bold 700', 'hongblog' );
			$l10n['bold-italic']           = esc_attr__( 'Bold 700 Italic', 'hongblog' );
			$l10n['extra-bold']            = esc_attr__( 'Extra-Bold 800', 'hongblog' );
			$l10n['extra-bold-italic']     = esc_attr__( 'Extra-Bold 800 Italic', 'hongblog' );
			$l10n['ultra-bold']            = esc_attr__( 'Ultra-Bold 900', 'hongblog' );
			$l10n['ultra-bold-italic']     = esc_attr__( 'Ultra-Bold 900 Italic', 'hongblog' );
			$l10n['invalid-value']         = esc_attr__( 'Invalid Value', 'hongblog' );

			return $l10n;

		}

		/**
		 * Hook to remove some controls
		 * 
		 * @param  WP_Customize
		 * @return void
		 */
		// private function _remove_controls( $wp_customize ) {

		// 	$controls = apply_filters('Hongblog_remove_customize_control', array() );

		// 	foreach ($controls as $control ) {

		// 		$wp_customize->remove_control($control);

		// 	}
		// }

		// /**
		//  * Get all of the shopper theme mods.
		//  * 
		//  *@since 1.0.0
		//  * @return array $Hongblog_theme_mods The shopper Theme Mods.
		//  */
		public function get_Hongblog_theme_mods() {
			$Hongblog_theme_mods = array(
				//'background_color'               => Hongblog_get_content_background_color(),
				'accent_color'                   => get_theme_mod( 'Hongblog_accent_color' ),
				'header_background_color'        => get_theme_mod( 'Hongblog_header_background_color' ),
				'header_link_color'              => get_theme_mod( 'Hongblog_header_link_color' ),
				'header_link_hover_color'        => get_theme_mod( 'Hongblog_header_link_hover_color' ),
				'header_text_color'              => get_theme_mod( 'Hongblog_header_text_color' ),
				'footer_background_color'        => get_theme_mod( 'Hongblog_footer_background_color' ),
				'widget_footer_background_color' => get_theme_mod( 'Hongblog_widget_footer_background_color' ),
				'footer_heading_color'           => get_theme_mod( 'Hongblog_footer_heading_color' ),
				'footer_text_color'              => get_theme_mod( 'Hongblog_footer_text_color' ),
				'text_color'                     => get_theme_mod( 'Hongblog_text_color' ),
				'heading_color'                  => get_theme_mod( 'Hongblog_heading_color' ),
				'button_background_color'        => get_theme_mod( 'Hongblog_button_background_color' ),
				'button_text_color'              => get_theme_mod( 'Hongblog_button_text_color' ),
				'button_alt_background_color'    => get_theme_mod( 'Hongblog_button_alt_background_color' ),
				'button_alt_text_color'          => get_theme_mod( 'Hongblog_button_alt_text_color' ),
			);

			return apply_filters( 'Hongblog_theme_mods', $Hongblog_theme_mods );
		}

		/**
		 * Get Customizer css.
		 *
		 * @see get_Hongblog_theme_mods()
		 * @since 1.0.0
		 * @return array $styles the css
		 */
		public function get_css() {
			$Hongblog_theme_mods = $this->get_Hongblog_theme_mods();
			$brighten_factor       = apply_filters( 'Hongblog_brighten_factor', 25 );
			$darken_factor         = apply_filters( 'Hongblog_darken_factor', -25 );

			$styles                = '
			.main-navigation ul li a,
			.site-title a,
			.site-branding h1 a,
			.site-footer .shopper-handheld-footer-bar a:not(.button) {
				color: ' . $Hongblog_theme_mods['header_link_color'] . ';
			}		


			@media screen and ( min-width: 768px ) {
				/*
				

				.secondary-navigation ul.menu a {
					color: ' . $Hongblog_theme_mods['header_text_color'] . ';
				}*/

		
			
			}';

			return apply_filters( 'Hongblog_customizer_css', $styles );
		}

		/**
		 * Get Customizer css associated with WooCommerce.
		 *
		 * @see get_Hongblog_theme_mods()
		 * @return array $woocommerce_styles the WooCommerce css
		 */
		public function get_woocommerce_css() {
			$Hongblog_theme_mods = $this->get_Hongblog_theme_mods();
			$brighten_factor       = apply_filters( 'Hongblog_brighten_factor', 25 );
			$darken_factor         = apply_filters( 'Hongblog_darken_factor', -25 );

			$woocommerce_styles    = '
						

			@media screen and ( min-width: 768px ) {
				.site-header-cart .widget_shopping_cart,
				.site-header .product_list_widget li .quantity {
					color: ' . $Hongblog_theme_mods['header_text_color'] . ';
				}
			}';

			return apply_filters( 'Hongblog_customizer_woocommerce_css', $woocommerce_styles );
		}

		/**
		 * Assign shopper styles to individual theme mods.
		 *
		 * @return void
		 */
		public function set_Hongblog_style_theme_mods() {
			set_theme_mod( 'Hongblog_styles', $this->get_css() );
			set_theme_mod( 'Hongblog_woocommerce_styles', $this->get_woocommerce_css() );
		}

		/**
		 * Add CSS in <head> for styles handled by the theme customizer
		 * If the Customizer is active pull in the raw css. Otherwise pull in the prepared theme_mods if they exist.
		 *
		 * @since 1.0.0
		 * @return void
		 */
		public function add_customizer_css() {
			$Hongblog_styles             = get_theme_mod( 'Hongblog_styles' );
			$Hongblog_woocommerce_styles = get_theme_mod( 'Hongblog_woocommerce_styles' );

			if ( is_customize_preview() || ( defined( 'WP_DEBUG' ) && true === WP_DEBUG ) || ( false === $Hongblog_styles && false === $Hongblog_woocommerce_styles ) ) {
				wp_add_inline_style( 'shopper-style', $this->get_css() );
				wp_add_inline_style( 'shopper-woocommerce-style', $this->get_woocommerce_css() );
			} else {
				wp_add_inline_style( 'shopper-style', get_theme_mod( 'Hongblog_styles' ) );
				wp_add_inline_style( 'shopper-woocommerce-style', get_theme_mod( 'Hongblog_woocommerce_styles' ) );
			}
		}		

		
		/**
		 * Get site logo.
		 *
		 * @since 1.0.0
		 * @return string
		 */
		public function get_site_logo() {
			return Hongblog_site_title_or_logo( false );
		}

		/**
		 * Get site name.
		 *
		 * @since 1.0.0
		 * @return string
		 */
		public function get_site_name() {
			return get_bloginfo( 'name', 'display' );
		}

		/**
		 * Get site description.
		 *
		 * @since 1.0.0
		 * @return string
		 */
		public function get_site_description() {
			return get_bloginfo( 'description', 'display' );
		}

		/**
		 * Remove Sidebar for one column layout
		 *
		 * @since 1.0.0
		 * @return void
		 */
		public function remove_sidebars() {
			// Shop Sidebar
			remove_action( 'Hongblog_shop_sidebar', 'Hongblog_shop_sidebar', 10 );

			// General Sidebar
			add_action( 'Hongblog_sidebar',        'Hongblog_get_sidebar',   10 );
		}

		/**
		 * Render homepage components
		 *
		 * @since 1.0.0
		 * @return void
		 */
		public function maybe_apply_render_homepage_component() {

			$options = get_theme_mod( 'Hongblog_homepage_control' );


			// Use pro options if it is available 
			if ( function_exists ('Hongblog_pro_get_homepage_hooks') ) {

				$options = Hongblog_pro_get_homepage_hooks();
			}

			$components = array();

			if ( isset( $options ) && '' != $options ) {
			
				$components = $options ;

				// Remove all existing actions on Hongblog_homepage.
				remove_all_actions( 'Hongblog_homepage' );

				foreach ($components as $k => $v) {

					if ( function_exists( $v ) ) {
							add_action( 'Hongblog_homepage', esc_attr( $v ), $k );
					}
				}

			}

		}

	}


endif;

return new Hongblog_Customizer();