<?php
/**
 * Latest Post Slider
 */
if ( ! function_exists( 'hongblog_latest_posts_slider' ) ) {

	function hongblog_latest_posts_slider( ) {

		$enable_slide = hongblog_get_theme_option( 'enable_slide' , 1 );

		if( $enable_slide == 1 ) :

		global $paged;
		global $wp_query;

		$paged = $wp_query->get( 'paged' );

		if ( ! $paged || $paged < 2 ) :

		//$theme_options = hongblog_theme_options();
		//$limit_slide = $theme_options['number_slide'];

		//$cats = $theme_options['show_slide_through_cat'];

		$args = array(
			'category__in' 		=> '', 
			'posts_per_page' 	=> 5
		);

		$slide_posts = new WP_Query( $args );

		if ( $slide_posts->have_posts() ) :
		?>
		<div class="feature-area">
			<div class="feature-wrap loading">
				<div id="slider" class="owl-carousel">
				<?php if ( have_posts() ) : ?>
					<?php 
					while ($slide_posts->have_posts() ) : $slide_posts->the_post();
						// if (has_post_thumbnail()) :						
					?>
					<div class="item">
						<a href="<?php the_permalink() ?>">
							<?php if ( has_post_thumbnail() ) : ?>
							<?php the_post_thumbnail('feature-thumbnails') ?>
							<?php else : ?>
								<img src="<?php echo get_template_directory_uri(); ?>/images/feature-thumbnails.jpg" />
							<?php endif; ?>

						</a>

						<?php 
							if($theme_options['disable_slide_date'] == 1 ) : 
								$modified_time = get_post_modified_time('F j, Y', null, $slide_posts->ID );
						?>

							<span class="date"><?php echo $modified_time; ?></span>

						<?php
							endif;
						?>

						<p class="flex-caption">
							<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
						</p>					  
					</div>

				<?php 
						// endif;
					endwhile;
				endif;
				wp_reset_query();
				?>

				
			</div>

				
		</div>

		<aside id="secondary" class="widget-area feature-sidebar">
			<?php dynamic_sidebar( 'feature-sidebar' ); ?>	
		</aside>	
	</div>
		<?php
			endif;
			wp_reset_postdata();

		endif;

	endif;

	}
}

/**
|------------------------------------------------------------------------------
| Excerpt
|------------------------------------------------------------------------------
|
*/

function hongblog_excerpt_length( $length ) {

	//$theme_options = hongblog_theme_options();
	$excerpt_length = hongblog_get_theme_option('excerpt_length' , 15);

	$number = intval ($excerpt_length) > 0 ?  intval ($excerpt_length) : $length;
	return $number;
}
add_filter( 'excerpt_length', 'hongblog_excerpt_length', 999 );

function hongblog_excerpt_more( $more ) {

	return hongblog_get_theme_option( 'excerpt_more' , '...' );

}
add_filter('excerpt_more', 'hongblog_excerpt_more');


function hongblog_yoast_breadcrumb() { 
	
	$yoast_breadcrumb = hongblog_get_theme_option('breadscrumb_yoast_seo', 1);

		if ( function_exists('yoast_breadcrumb') && $yoast_breadcrumb == 1) {
		  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
		}
 }