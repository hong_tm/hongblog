<?php
/**
 * Shopper Pro Customize Options
 * 
 * @package Az_Authority
 */

/**
 * Default Sections
 * 
 * @return array
 */
function hongblog_get_default_sections( $plugin_token ) {

	$panel_id = $plugin_token . '_panel_id';
	$prefix = $plugin_token . '_section_';

	$sections = apply_filters( $plugin_token . '_default_sections', array (

		$prefix . 'general'	=> array(
			'title'			=> __( 'General Settings', 'hogblog' ),
			'panel'			=> $panel_id,
			'priority'		=> 15,
		),

		$prefix . 'single'	=> array(
			'title'			=> __( 'Single Settings', 'hongblog' ),
			'panel'			=> $panel_id,
			'priority'		=> 40,
		),

		$prefix . 'slide'	=> array(
			'title'			=> __( 'Slide Settings', 'hongblog' ),
			'panel'			=> $panel_id,
			'priority'		=> 45,
		),

		$prefix . 'style'	=> array(
			'title'			=> __( 'Style Settings', 'hongblog' ),
			'panel'			=> $panel_id,
			'priority'		=> 50,
		),

		$prefix . 'social'	=> array(
			'title'			=> __( 'Social Settings', 'hongblog' ),
			'panel'			=> $panel_id,
			'priority'		=> 55,
		),

		$prefix . 'footer'	=> array(
			'title'			=> __( 'Footer Settings', 'hongblog' ),
			'panel'			=> $panel_id,
			'priority'		=> 60,
		),

		$prefix . 'typography'	=> array(
			'title'			=> __( 'Typography Settings', 'hongblog' ),
			'panel'			=> $panel_id,
			'priority'		=> 70,
		),

	));

	return $sections;
}


/**
 * Default Fields
 */
function hongblog_get_default_fields( $plugin_token ) {

	// Customizer Asset Path
	$asset_customizer_path = get_template_directory_uri() .'/assets';

	$prefix_section = $plugin_token . '_section_';

	$fields = apply_filters( $plugin_token . '_default_fields', array(

		/**
		* General Fields
		*/
		array(
			'settings'			=> 'sidebar_layout',
			'label'				=> __( 'Sidebar', 'hongblog' ),
			'section'			=> $prefix_section . 'general',
			'type'				=> 'radio-image',
			'default'			=> 'right',
			'priority'			=> 1,
			'choices'			=> array(
				'left'			=> $asset_customizer_path .'/images/col-2cr.png',				
				'right'			=> $asset_customizer_path .'/images/col-2cl.png',
			)
		),

		// Grid Post 

		array(
			'settings'			=> 'post_layout',
			'label'				=> __( 'Post Layout', 'hongblog' ),
			'section'			=> $prefix_section . 'general',
			'type'				=> 'radio-image',
			'default'			=> 'list',
			'priority'			=> 5,
			'choices'			=> array(
				'list'			=> $asset_customizer_path .'/images/post-list.png',				
				'grid'			=> $asset_customizer_path .'/images/post-grid.png',
			)
		),

		// Meta info Single Posts
		array(
			'settings'			=> 'meta_info',
			'label'				=> __( 'Single Meta Info', 'hongblog' ),
			'section'			=> $prefix_section . 'general',
			'type'				=> 'multicheck',
			'default'			=> array('date', 'author', 'category', 'tag'),
			'priority'			=> 10,
			'choices'			=> array(				
				'author'			=> esc_attr__( 'Author', 'hongblog' ),
				'date'				=> esc_attr__( 'Date', 'hongblog' ),
				'category'			=> esc_attr__( 'Categories', 'hongblog' ),
				'tag'				=> esc_attr__( 'Tags', 'hongblog' ),
			)
		),

		//Sticky Menu
		array(
			'settings'			=> 'sticky_menu',
			'label'				=> __( 'Sticky Menu', 'hongblog' ),
			'section'			=> $prefix_section . 'general',
			'type'				=> 'switch',
			'default'			=> '1',
			'priority'			=> 20,
		),		
		//Back To Top
		array(
			'settings'			=> 'back_to_top',
			'label'				=> __( 'Back To Top', 'hongblog' ),
			'section'			=> $prefix_section . 'general',
			'type'				=> 'switch',
			'default'			=> '1',
			'priority'			=> 30,
		),	

		//Full //Excerpt
		array(
			'settings'			=> 'post_length',
			'label'				=> __( 'Post Length', 'hongblog' ),
			'section'			=> $prefix_section . 'general',
			'type'				=> 'radio',
			'default'			=> 'excerpt',
			'priority'			=> 40,
			'choices'     => array(
				'full_post' => __( 'Show full posts', 'hongblog' ),
				'excerpt' => __( 'Show post excerpts', 'hongblog' ),
				),
		),	

		//Excerpt Length
		array(
			'settings'			=> 'excerpt_length',
			'label'				=> __( 'Excerpt Length', 'hongblog' ),
			'section'			=> $prefix_section . 'general',
			'type'				=> 'number',
			'default'			=> 15,
			'priority'			=> 50,
			'active_callback'	=> array(
					array(
						'setting'		=> 'post_length',
						'operator'		=> '==',
						'value'			=> 'excerpt',
					),
				),
		),

		// Excerpt More Ending Text
		array(
			'settings'			=> 'excerpt_more',
			'label'				=> __( 'Excerpt More Ending Text', 'hongblog' ),
			'section'			=> $prefix_section . 'general',
			'type'				=> 'text',
			'default'			=> '...',
			'priority'			=> 60,
			'active_callback'	=> array(
					array(
						'setting'	=> 'post_length',
						'operator'	=> '==',
						'value'		=> 'excerpt',
					),
				),
		),

		// Pagination Styles
		// array(
		// 	'settings'			=> 'paging',
		// 	'label'				=> __( 'Pagination Styles', 'hongblog' ),
		// 	'section'			=> $prefix_section . 'general',
		// 	'type'				=> 'radio',
		// 	'default'			=> 'paging-numberal',
		// 	'priority'			=> 70,
		// 	'choices'			=> array(
		// 		'paging-default'	=> esc_attr__('Default (Older Posts/Newer Posts)','hongblog'),
		// 		'paging-numberal'	=> esc_attr__('Numberal (1 2 3 ...)','hongblog'),
		// 		'paging-loading'	=> esc_attr__('Load More...','hongblog'),
		// 		'paging-infinite'	=> esc_attr__('Auto Infinite Scroll','hongblog'),
		// 	),
		// ),

		// Header Code
		array(
			'settings'			=> 'header_code',
			'label'				=> __( 'Header Code (wp_head)', 'hongblog' ),
			'section'			=> $prefix_section . 'general',
			'type'				=> 'cutom-code',
			'sanitize_callback'	=> 'sanitize_textarea_code',
			'priority' 			=> 100,
			),

		// Footer Code
			array(
			'settings'			=> 'footer_code',
			'label'				=> __( 'Footer Code (wp_footer)', 'hongblog' ),
			'section'			=> $prefix_section . 'general',
			'type'				=> 'cutom-code',
			'sanitize_callback'	=> 'sanitize_textarea_code',
			'default'			=> '',
			'priority' 			=> 110,
			),

		/**
		* Single Setting
		*/

		// Breadscrumb
		array(
			'settings'				=> 'breadscrumb_yoast_seo',
			'label'					=> __( 'Use Yoast SEO Breadscrumb', 'hongblog' ),
			'section'				=> $prefix_section . 'single',
			'type'					=> 'switch',
			'default'				=> 0,
			'priority'				=> 10,

		),

		// Post Navigation
		array(
			'settings'				=> 'post_nav',
			'label'					=> __( 'Post Navigation', 'hongblog' ),
			'section'				=> $prefix_section . 'single',
			'type'					=> 'switch',
			'default'				=> 1,
			'priority'				=> 30,

		),

		// Related Posts
		array(
			'settings'				=> 'related_posts',
			'label'					=> __( 'Related Post', 'hongblog' ),
			'section'				=> $prefix_section . 'single',
			'type'					=> 'switch',
			'default'				=> 1,
			'priority'				=> 60,

		),
		// Number Related Posts
		array(
			'settings'				=> 'number_related_post',
			'label'					=> __( 'Number Related Posts', 'hongblog' ),
			'section'				=> $prefix_section . 'single',
			'type'					=> 'number',
			'default'				=> 6,
			'priority'				=> 70,
			'active_callback'	=> array(
					array(
						'setting'	=> 'related_posts',
						'operator'	=> '==',
						'value'		=> 1,
					)
				)
			
		),

		// Related Posts Taxonomy
		array(
			'settings'				=> 'related_post_taxonomy',
			'label'					=> __( 'Related Post Taxonomy', 'hongblog' ),
			'section'				=> $prefix_section . 'single',
			'type'					=> 'radio',
			'default'				=> 'cat',
			'priority'				=> 80,
			'choices'				=> array(
				'cat' => esc_html__( 'Categories', 'hongblog' ),
				'tag' => esc_html__( 'Tags', 'hongblog' )
			),
			'active_callback'	=> array(
					array(
						'setting'	=> 'related_posts',
						'operator'	=> '==',
						'value'		=> 1,
					)
				)
		),

		// Display Related Posts
		array(
			'settings'				=> 'related_post_display_style',
			'label'					=> __( 'Related Post Display Style', 'hongblog' ),
			'section'				=> $prefix_section . 'single',
			'type'					=> 'radio',
			'default'				=> 'grid',
			'priority'				=> 90,
			'choices'				=> array(
				'grid'	=> esc_html__( 'Thumbnail and title', 'hongblog' ),
				'list'	=> esc_html__( 'Only tittle, in list style', 'hongblog' )
			),
			'active_callback'	=> array(
					array(
						'setting'	=> 'related_posts',
						'operator'	=> '==',
						'value'		=> 1,
					)
				)
		),


		/**
		*Slide Setting
		*/

		// Disable slider
		array(
			'settings'			=> 'enable_slide',
			'label'				=> __( 'Enable Slide', 'hongblog' ),
			'section'			=> $prefix_section . 'slide',
			'type'				=> 'switch',
			'default'			=> 1,
			'priority'			=> 20,

		),

		/**
		* Style Setting
		*/

		// Custom Style
		array(
			'settings'			=> 'enable_custom_style',
			'label'				=> __( 'Enable Custom Style', 'hongblog' ),
			'section'			=> $prefix_section . 'style',	
			'type'				=> 'switch',
			'default'			=> 0,
			'priority'			=> 10,
		),

		array(
			'type'			=> 'color',
			'settings'		=> 'primary_color',
			'label'			=> __( 'Primary Color', 'hongblog' ),
			'section'		=> $prefix_section . 'style',
			'default'		=> '#fc9618',
			'priority'		=> 20,
			'alpha'			=> true,
			'active_callback'	=> array(
				array(
					'setting'	=> 'enable_custom_style',
					'operator'	=> '==',
					'value'		=> 1,
				),
			),
		),

		array(
			'type'			=> 'color',
			'settings'		=> 'secondary_color',
			'label'			=> __( 'Secondary Color', 'hongblog' ),
			'section'		=> $prefix_section . 'style',
			'default'		=> '#1b2835',
			'priority'		=> 30,
			'alpha'			=> true,
			'active_callback'	=> array(
				array(
					'setting'	=> 'enable_custom_style',
					'operator'	=> '==',
					'value'		=> 1,
				),
			),
		),


		/**
		 * Add the social buttons section
		 */

		// Social Sharing Button
		array(
			'settings'			=> 'social_sharing_button',
			'label'				=> __( 'Social Sharing Button', 'hongblog' ),
			'section'			=> $prefix_section . 'social',
			'type'				=> 'switch',
			'default'			=> '0',
			'priority'			=> 10,

		),

		// Social Sharing Button Position
		array(
			'type'			=> 'multicheck',
			'settings'		=> 'sharing_button_position',
			'label'			=> esc_attr__( 'Social Sharing Button Positions', 'hongblog' ),
			'section'		=> $prefix_section . 'social',
			'default'		=> array('sticky-left'),
			'priority'		=> 30,
			'choices'		=> array(
				'before-content'		=> esc_attr__( 'Before content', 'hongblog' ),
				'after-content'			=> esc_attr__( 'After content', 'hongblog' ),
				'sticky-left'			=> esc_attr__( 'Sticky left', 'hongblog' ),
			),
			'active_callback'	=> array(
				array(
					'setting'	=> 'social_sharing_button',
					'operator'	=> '==',
					'value'		=> 1,
				)
			)
		),

		// Check to enable social buttons
		array(
			'type'			=> 'multicheck',
			'settings'		=> 'enable_social_sharing_button',
			'label'			=> esc_attr__( 'Enable Social Buttons', 'hongblog' ),
			'section'		=> $prefix_section . 'social',
			'default'		=> array('twitter', 'facebook', 'googleplus'),
			'priority'		=> 40,
			'choices'		=> array(
				'twitter'			=> esc_attr__( 'Twitter', 'hongblog' ),
				'facebook'			=> esc_attr__( 'Facebook', 'hongblog' ),
				'googleplus'		=> esc_attr__( 'Google Plus', 'hongblog' ),
				'linkedin'			=> esc_attr__( 'Linkedin', 'hongblog' ),
				'pinterest'			=> esc_attr__( 'Pinterest', 'hongblog' ),
			),
			'active_callback'	=> array(
				array(
					'setting'	=> 'social_sharing_button',
					'operator'	=> '==',
					'value'		=> 1,
				)
			)
		),

		/**
		* Footer Fields
		*/
		
		array(
			'settings'			=> 'hongblog_title_head_footer',
			'label'				=> __( 'Footer Widget Titles', 'hongblog' ),
			'section'			=> $prefix_section . 'footer',
			'type'				=> 'color',
			'default'			=> '#000',
			'priority'			=> 20,
			'choices'			=> array(
				'alpha'			=> true,
			),
			'output'			=> array(
				array(
					'element'	=> '.footer-widgets .widget-title',
					'property'	=> 'color'
				)
			)
		),

		array(
			'settings'			=> 'hongblog_link_color_footer',
			'label'				=> __( 'Footer Widget Links Color', 'hongblog' ),
			'section'			=> $prefix_section . 'footer',
			'type'				=> 'color',
			'default'			=> '#000',
			'priority'			=> 40,
			'choices'			=> array(
				'alpha'			=> true,
			),
			'output'			=> array(
				array(
					'element'	=> '.footer-widgets a',
					'property'	=> 'color'

				)
			)
		),

		array(
			'settings'			=> 'footer_widget_col',
			'label'				=> __( 'Footer Widget Columns', 'hongblog' ),
			'section'			=> $prefix_section . 'footer',
			'type'				=> 'radio-image',
			'default'			=> 'three-col',
			'priority'			=> 10,
			'choices'			=> array(
					'none-col'		=> $asset_customizer_path .'/images/layout-off.png',
					'two-col'		=> $asset_customizer_path .'/images/footer-widgets-2.png',
					'three-col'		=> $asset_customizer_path .'/images/footer-widgets-3.png',
			),

		),

		array(
			'settings'			=> 'footer_copyright_text',
			'label'				=> __( 'Footer Copyright Text', 'hongblog' ),
			'section'			=> $prefix_section . 'footer',
			'type'				=> 'textarea',
			'default'			=> '',
			'priority'			=> 50,
			
		),	


		/**
		* Typography Fields
		*/


		array(
			'type'			=> 'typography',
			'settings'		=> 'title_typography2',
			'label'			=> esc_attr__( 'Headings & Title', 'hongblog' ),
			'description'	=> esc_attr__( 'Select the typography options for your headings.', 'hongblog' ),
			'help'			=> esc_attr__( 'The typography options you set here apply to headings on your site.', 'hongblog' ),
			'section'		=> $prefix_section . 'typography',
			'priority'		=> 10,
			'default'		=> array(
				'font-family'		=> 'Roboto',
				'variant'			=> '400',
			),
			'output' => array(
				array(
					'element'	=> array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6' ),
				),
			),
		),

		//Add the body-typography control
		array(
			'type'			=> 'typography',
			'settings'		=> 'body_typography2',
			'label'			=> esc_attr__( 'Body Fonts', 'hongblog' ),
			'description'	=> esc_attr__( 'Select the main typography options for your site.', 'hongblog' ),
			'help'			=> esc_attr__( 'The typography options you set here apply to all content on your site.', 'hongblog' ),
			'section'		=> $prefix_section . 'typography',
			'priority'		=> 20,
			'default'		=> array(
				'font-family'		=> 'Roboto',
				'font-size'			=> '15px',
				'color'				=> '#222222',
				'variant'			=> '400',
			),
			'output' => array(
				array(
					'element'	=> 'body',
				),
			),
		),


	));

	return $fields;
}