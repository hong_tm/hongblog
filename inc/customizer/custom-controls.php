<?php

/**
 * Custom Controls for the Customizer
 *
 * Register new Control Type for Kirki
 */

add_action( 'customize_register', function( $wp_customize ) {
	/**
	 * The custom code control class
	 */
	class Themecountry_Kirki_Controls_Custom_Code_Control extends WP_Customize_Control {
		public $type = 'cutom-code';
		public function render_content() { 

			?>
			<label>
                <?php if ( ! empty( $this->label ) ) : ?>
                    <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
                <?php endif;
                if ( ! empty( $this->description ) ) : ?>
                    <span class="description customize-control-description"><?php echo $this->description; ?></span>
                <?php endif; ?>
                <textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
            </label>
			<?php
			
		}
	}
	// Register our custom control with Kirki
	add_filter( 'kirki/control_types', function( $controls ) {
		$controls['cutom-code'] = 'Themecountry_Kirki_Controls_Custom_Code_Control';
		return $controls;
	} );

} );