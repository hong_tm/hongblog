<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package hongblog
 */

if ( ! function_exists( 'hongblog_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function hongblog_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( '%s', 'post author', 'hongblog' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"><i class="fa fa-user"></i> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'hongblog_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function hongblog_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

		<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
			<?php
			the_post_thumbnail( 'post-thumbnail', array(
				'alt' => the_title_attribute( array(
					'echo' => false,
				) ),
			) );
			?>
		</a>

		<?php
		endif; // End is_singular().
	}
endif;

if ( ! function_exists('hongblog_header_title') ) :
	function hongblog_header_title() {

		$logo = get_theme_mod('custom_logo');

		?>
			<?php if ( !empty($logo) ) : ?>	
					<?php if( (is_front_page() || is_home()) && function_exists( 'the_custom_logo' ) ) : ?>
					<h1 class="site-title logo" itemprop="headline">
						<?php  the_custom_logo() ?>
					</h1>				
					<?php else : ?>
						<?php if ( function_exists( 'the_custom_logo' ) ): ?>		
							<h2 class="site-title logo" itemprop="headline">
								<?php

	      							the_custom_logo();

								?>
							</h2>
						<?php endif; ?>
				<?php endif ?>
				
			<?php else : ?>	
				<div class="site-title">			
					<?php if( is_front_page() || is_home() ) : ?>
						<h1 class="title-logo" itemprop="headline" class="site-title">
							<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php echo esc_attr(get_bloginfo( 'description' )); ?>">
								<?php bloginfo( 'name' ); ?>
							</a>
						</h1>
						<h2 class="title-description"><?php bloginfo( 'description' ); ?></h2>
				<?php else : ?>
						<h2 class="title-logo">
							<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php echo esc_attr(get_bloginfo( 'description' )); ?>">
							<?php bloginfo( 'name' ); ?>
							</a>
						</h2>
						<h3 class="title-description"><?php bloginfo( 'description' ); ?></h3>
					<?php endif ?>
				</div>
			<?php endif ?>
		<?php
	}
endif;


if ( ! function_exists( 'hongblog_paging_nav' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 */
function hongblog_paging_nav() {
	

	//$theme_options = hongblog_theme_options();

	$nav_style =  'pageing-numberal';

	if  ( $nav_style == 'pageing-numberal') :
		// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( '<i class="fa fa-angle-left"></i>', 'hongblog' ),
				'next_text'          => __( '<i class="fa fa-angle-right"></i>', 'hongblog' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'hongblog' ) . ' </span>',
			) );
	else :

		$args = array(
			'prev_text'          => __( '<i class="fa fa-angle-left"></i> Previous', 'hongblog' ),
			'next_text'          => __( 'Next <span class="meta-nav"><i class="fa fa-angle-right"></i>', 'hongblog' ),
		);

	  the_posts_navigation( $args );

	endif;
}
endif;

if ( ! function_exists( 'hongblog_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function hongblog_posted_on() {

	$meta_items = hongblog_get_theme_option('meta_info', array());
	if(empty($meta_items))
		return;

	if(is_array($meta_items))
		$item = array_flip($meta_items);

	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {

		if( $theme_options['metainfo_published_update'] ) {
			$time_string = '<time class="updated" datetime="%3$s">%4$s</time>';
		} else {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
		}
		//$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( '%s ', 'post date', 'hongblog' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( '%s', 'post author', 'hongblog' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);	
	if ( isset( $item['author'] ) ) :
		echo '<span class="byline"><i class="fa fa-user" aria-hidden="true"></i>' . $byline . '</span>'; // WPCS: XSS OK.
	endif;
	if ( isset( $item['category'] ) ) :
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'hongblog' ) );
			if ( $categories_list ) {
				printf( '<span class="cat-links"><i class="fa fa-archive"></i>' . esc_html__( '%1$s', 'hongblog' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}
		}
	endif;
	

	if ( isset( $item['date'] ) ) : 
			echo '<span class="posted-on"><i class="fa fa-calendar" aria-hidden="true"></i>' . $posted_on . '</span>';
	endif;

	if ( isset($item['tag']) && 'post' === get_post_type() ) {			

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html__( ', ', 'hongblog' ) );
			if ( $tags_list ) {
				printf( '<span class="tags-links"><i class="fa fa-tag" aria-hidden="true"></i>' . esc_html__( '%1$s', 'hongblog' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}

}
endif;

if ( ! function_exists( 'hongblog_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function hongblog_entry_footer() {

	//$theme_options = hongblog_theme_options();
	//$meta_items = array_flip($theme_options['post_meta_info']);

	//if ( isset( $meta_items['meta-tag'] ) ) :
		// Hide category and tag text for pages.
		
	//endif;
	
	//if ( isset( $meta_items['meta-comment'] ) ) :
		if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link"><i class="fa fa-comment" aria-hidden="true"></i>';
			/* translators: %s: post title */
			comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'hongblog' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
			echo '</span>';
		}
	//endif;

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'hongblog' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;
